import React, { FC } from 'react';
import { LogBox } from 'react-native';
import Route from './src/route';

const App: FC = () => {
  LogBox.ignoreLogs(['Warning: ...']);
  LogBox.ignoreAllLogs();

  return <Route />;
};

export default App;