import {all} from 'redux-saga/effects';
import sagaProducts from './modules/products/saga';
export default function* rootSaga() {
  yield all([sagaProducts()]);
}
