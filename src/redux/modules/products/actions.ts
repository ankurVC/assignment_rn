import {PRODUCTS_REQUEST, PRODUCTS_SUCCESS, PRODUCTS_FAILURE} from './types';
import {
  ProductsRequest,
  ProductsSuccess,
  ProductsSuccessPayload,
  ProductsFailure,
} from './actionType';

export const productsRequest = (): ProductsRequest => ({
  type: PRODUCTS_REQUEST,
});

export const productsSuccess = (
  payload: ProductsSuccessPayload,
): ProductsSuccess => ({
  type: PRODUCTS_SUCCESS,
  payload,
});

export const productsFailure = (): ProductsFailure => ({
  type: PRODUCTS_FAILURE,
});
