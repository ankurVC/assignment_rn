import {PRODUCTS_REQUEST, PRODUCTS_SUCCESS, PRODUCTS_FAILURE} from './types';

export type ProductsStateParmas = {
  payload: any;
};
export interface ProductsSuccessPayload {
  payload: any;
}

export type ProductsRequest = {
  type: typeof PRODUCTS_REQUEST;
};
export type ProductsSuccess = {
  type: typeof PRODUCTS_SUCCESS;
  payload: ProductsSuccessPayload;
};
export type ProductsFailure = {
  type: typeof PRODUCTS_FAILURE;
};

export type ProductsActions =
  | ProductsRequest
  | ProductsSuccess
  | ProductsFailure;
