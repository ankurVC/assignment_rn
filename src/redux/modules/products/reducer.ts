import {PRODUCTS_REQUEST, PRODUCTS_SUCCESS, PRODUCTS_FAILURE} from './types';
import {ProductsActions, ProductsStateParmas} from './actionType';

const initialState: ProductsStateParmas = {
  payload: null,
};

export default (state = initialState, action: ProductsActions) => {
  switch (action.type) {
    case PRODUCTS_REQUEST:
      return {
        ...state,
      };
    case PRODUCTS_SUCCESS:
      return {
        ...state,
        payload: action.payload,
      };
    case PRODUCTS_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
