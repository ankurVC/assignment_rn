import {put, takeEvery} from 'redux-saga/effects';
import {productsSuccess, productsFailure} from './actions';
import {PRODUCTS_REQUEST} from './types';
import {Request} from '../../../services';
import Config from '../../../constants/Config';

function* onProductsRequest(): Generator<any> {
  try {
    const response: any = yield Request.get(Config.PRODUCTS);
    if (response.data && response?.data?.products?.length > 0) {
      yield put(productsSuccess(response?.data?.products));
    } else {
      yield put(productsFailure());
    }
  } catch (e) {
    console.log('Error in onProductsRequest ', e);
  }
}

function* sagaProducts() {
  yield takeEvery(PRODUCTS_REQUEST, onProductsRequest);
}
export default sagaProducts;
