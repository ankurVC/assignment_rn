import {legacy_createStore as createStore, applyMiddleware} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistReducer} from 'redux-persist';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import Config from './../constants/Config';
import reducers from './reducers';
import rootSaga from './sagas';

/**
 * Create Axios Client to communicate
 *
 */

const axiosClient = axios.create({
  baseURL: Config.API_URL,
  responseType: 'json',
});
// Store instance
let store: any = null;
let persistor = null;

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware();

/**
 * Create the Redux store
 */

export const configureStore = () => {
  store = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleware, axiosMiddleware(axiosClient)),
  );
  sagaMiddleware.run(rootSaga);
  persistor = persistStore(store);
  return {store, persistor};
};

/**
 * Dispatch an action
 */
export const dispatch = (args: any) => store.dispatch(...args);

export default {
  dispatch,
  configureStore,
};
