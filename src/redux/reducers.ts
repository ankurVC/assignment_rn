import {combineReducers} from 'redux';
import productsReducer from './modules/products/reducer';
const appReducer = combineReducers({
  productsReducer,
});

const rootReducer = (state: any, action: any) => {
  return appReducer(state, action);
};

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
