import React, {FC, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AppStack} from './navigations';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {configureStore} from './redux';
const {store, persistor} = configureStore();

const Route: FC = () => {
  //  const [user, setUser] = useState<any>('Admin');
  useEffect(() => {}, []);

  return (
    <NavigationContainer>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppStack />
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
};

export default Route;
