import {StyleSheet} from 'react-native';
import Colors from '../../constants/Colors';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../../constants/Dimentions';

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: wp(88),
    height: hp(8),
    marginTop: 20,
  },

  titleText: {
    color: Colors.black,
    fontSize: 20,
    fontWeight: 'bold',
  },
  leftIcon: {
    resizeMode: 'contain',
  },
  rightIcon: {
    resizeMode: 'contain',
  },
});

export default Styles;
