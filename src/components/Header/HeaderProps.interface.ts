interface HeaderProps {
  onPressBack?: Function;
  headerTitle?: string | null | undefined;
  rightImage?: _SourceUri;
  leftImage?: _SourceUri;
  isPrimary?: boolean;
}

export default HeaderProps;
