import React, {FC} from 'react';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import styles from './Style';

import {Images, Colors} from '../../constants';
import HeaderProps from './HeaderProps.interface';
import Styles from './Style';

const Header: FC<HeaderProps> = ({
  onPressBack,
  headerTitle,
  leftImage,
  rightImage,
  isPrimary,
}) => {
  const {leftIcon, rightIcon} = styles;
  return (
    <View style={Styles.container}>
      <TouchableOpacity onPress={() => (onPressBack ? onPressBack() : null)}>
        {leftImage ? (
          <Image
            resizeMode="contain"
            source={Images.arrow_left}
            style={[
              leftIcon,
              {tintColor: isPrimary ? Colors.primaryColor : Colors.black},
            ]}
          />
        ) : (
          <View />
        )}
      </TouchableOpacity>
      <Text style={Styles.titleText}>{headerTitle}</Text>
      {rightImage ? (
        <TouchableOpacity onPress={() => (onPressBack ? onPressBack() : null)}>
          <Image resizeMode="contain" source={rightImage} style={[rightIcon]} />
        </TouchableOpacity>
      ) : (
        <View />
      )}
    </View>
  );
};

export default Header;
