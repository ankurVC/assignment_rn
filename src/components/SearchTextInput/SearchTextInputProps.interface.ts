import {ReturnKeyTypeOptions, TextStyle, ViewStyle} from 'react-native';

interface SearchTextInputProps {
  refs?: any;
  value?: string;
  onChangeText?: any;
  searchTileStyle?: ViewStyle;
  inputStyle?: TextStyle;
  inputLabel?: string;
  isDisabled?: boolean;
  returnKeyType?: ReturnKeyTypeOptions;
  isSecure?: boolean;
  isMultine?: boolean;
  placholderText: string;
  rightIcon?: _SourceUri;
  onclose?: any;
}
export default SearchTextInputProps;
