import React, {FC} from 'react';
import {TextInput, View, Image} from 'react-native';
import Styles from './Styles';
import SearchTextInputProps from './SearchTextInputProps.interface';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Images} from '../../constants';
const SearchTextInput: FC<SearchTextInputProps> = ({
  refs,
  value,
  onChangeText,
  inputStyle,
  searchTileStyle,
  isDisabled,
  returnKeyType,
  isSecure,
  isMultine,
  placholderText,
  rightIcon,
  onclose,
}) => {
  return (
    <View style={[Styles.searchContainer, searchTileStyle]}>
      <TextInput
        ref={refs}
        onChangeText={(text: string) => {
          onChangeText(text);
        }}
        style={[Styles.textInputStyle, inputStyle]}
        secureTextEntry={isSecure}
        editable={isDisabled}
        value={value}
        placeholder={placholderText}
        multiline={isMultine}
        returnKeyType={returnKeyType}
      />
      {value && value.length > 0 ? (
        <TouchableOpacity onPress={() => onclose?.()}>
          <Image source={Images.close_icon} style={Styles.righIconStyle} />
        </TouchableOpacity>
      ) : rightIcon ? (
        <Image source={rightIcon} style={Styles.righIconStyle} />
      ) : null}
      {}
    </View>
  );
};

export default SearchTextInput;
