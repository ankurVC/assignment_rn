import {StyleSheet} from 'react-native';
import {Colors} from '../../constants';
import {widthPercentageToDP as wp} from '../../constants/Dimentions';
const styles = StyleSheet.create({
  searchContainer: {
    width: wp(90),
    height: 50,
    backgroundColor: Colors.white,
    borderRadius: 20,
    paddingHorizontal: 15,
    shadowColor: Colors.shadowColor,
    shadowOffset: {height: 1, width: 1},
    shadowRadius: 2,
    shadowOpacity: 1,
    elevation: 3,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  textInputStyle: {
    fontSize: 14,
    fontWeight: '500',
  },
  righIconStyle: {
    height: 19,
    width: 19,
    tintColor: Colors.black,
  },

  errorTextStyle: {
    fontSize: 12,
    color: Colors.primaryColor,
  },
});

export default styles;
