import React, { FC } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Products, ProductDetail } from '../screens';
import { AppStackProps } from '../@types';

const AppStack: FC = () => {
  const AppStack = createNativeStackNavigator<AppStackProps>();

  return (
    <>
      <AppStack.Navigator screenOptions={{ headerShown: false }}>
        <AppStack.Screen name="Products" component={Products} />
        <AppStack.Screen name="ProductDetail" component={ProductDetail} />

      </AppStack.Navigator>
    </>
  );
};

export default AppStack;
