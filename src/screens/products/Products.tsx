import React, {FC, useEffect, useState, useRef} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationScreens} from '../../navigations/NavigationScreens';
import {AppStackProps} from '../../@types';
import {productsRequest} from '../../redux/modules/products/actions';
import {RootState} from '../../redux/reducers';
import {CategoryCard, ItemCard} from './templates';
import {Header, SearchTextInput} from '../../components';

import Styles from './Styles';
import {Images} from '../../constants';

type ProductsScreenNavigationType = NativeStackNavigationProp<
  AppStackProps,
  NavigationScreens.Products
>;

const Products: FC = () => {
  const navigation = useNavigation<ProductsScreenNavigationType>();
  const dispatch = useDispatch();
  const {productData} = useSelector((state: RootState) => ({
    productData: state?.productsReducer?.payload,
  }));

  const listRef = useRef<FlatList>(null);
  const [categories, setCategories] = useState<any>([]);
  const [selectedCategories, setSelectedCategories] = useState<any>([]);
  const [data, setData] = useState<any>(productData);
  const [value, setValue] = useState('');
  const [isFilter, setFilter] = useState<boolean>(false);

  useEffect(() => {
    dispatch(productsRequest());
    getCategories();
  }, [dispatch]);

  const getCategories = () => {
    const catData = productData.map((item: any) => item.category);
    const unique = [...new Set(catData)];
    const updatedCat = unique.map((item: any) => ({
      category: item,
      isSelected: false,
    }));
    setCategories(updatedCat);
  };

  const handleSelectCategory = (category: string) => {
    setCategories(
      categories.map((item: any) => {
        if (item.category === category) {
          return {
            ...item,
            isSelected: !item.isSelected,
          };
        } else {
          return {
            ...item,
            isSelected: false,
          };
        }
      }),
    );
    setSelectedCategories(category);
    filterListData(category);
  };

  const filterListData = (category: string) => {
    const filteredCat = productData.filter(
      (item: any) => item.category == category,
    );
    setData(filteredCat);
  };

  const filterFromSearchText = (searchText: string) => {
    if (selectedCategories != '') {
      const filterData = productData
        .filter((item: any) => item.category == selectedCategories)
        .filter((item: any) => {
          const itemData1 = `
        ${item.title?.toUpperCase()} ||
        ${item.title?.toLowerCase()}||`;
          const textData1 = searchText.toUpperCase();
          return itemData1.indexOf(textData1) > -1;
        });
      setData(filterData);
    } else {
      const filterData = productData.filter((item: any) => {
        const itemData1 = `
      ${item.title?.toUpperCase()} ||
      ${item.title?.toLowerCase()}||`;
        const textData1 = searchText.toUpperCase();
        return itemData1.indexOf(textData1) > -1;
      });
      setData(filterData);
    }
  };
  const onFilter = () => {
    setFilter(true);
  };

  const onClear = () => {
    setFilter(false);
    setData(productData);
    getCategories();
    setSelectedCategories('');
  };
  const onItemPress = (itemData: any) => {
    navigation.navigate(NavigationScreens.ProductDetail, {item: itemData});
  };

  const renderCategoryItem = ({item}: any) => {
    return <CategoryCard item={item} onPress={handleSelectCategory} />;
  };
  const emptyView = () => {
    return (
      <View style={Styles.emptyTile}>
        <Text style={Styles.emptyText}>No Result found!</Text>
      </View>
    );
  };
  const renderItem = ({item, index}: any) => {
    return (
      <ItemCard
        item={item}
        index={index}
        onPress={(itemData: any) => onItemPress(itemData)}
      />
    );
  };
  return (
    <View style={Styles.container}>
      <Header headerTitle={'Products'} />
      <View style={Styles.searchContainer}>
        <SearchTextInput
          onChangeText={(text: string) => {
            setValue(text);
            filterFromSearchText(text);
          }}
          onclose={() => {
            setValue('');
            setData(productData);
            filterFromSearchText('');
          }}
          value={value}
          placholderText="Search"
          rightIcon={Images.search_icon}
          searchTileStyle={Styles.searchTile}
        />
        {isFilter ? (
          <TouchableOpacity onPress={() => onClear?.()}>
            <Text style={Styles.clear}>Clear</Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => onFilter?.()}>
            <Image source={Images.filter_icon} style={Styles.righIconStyle} />
          </TouchableOpacity>
        )}
      </View>
      {data && data.length > 0 && isFilter && (
        <FlatList
          data={categories}
          keyExtractor={(item: any) => item?.id?.toString()}
          renderItem={renderCategoryItem}
          contentContainerStyle={Styles.Categorylist}
          scrollEnabled={true}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      )}
      <FlatList
        style={Styles.dataList}
        ref={listRef}
        data={data}
        extraData={data}
        keyExtractor={(item: any) => item?.id.toString()}
        renderItem={renderItem}
        scrollEnabled={true}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={emptyView}
      />
    </View>
  );
};

export default Products;
