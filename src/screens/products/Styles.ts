import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from '../../constants/Dimentions';
import {Colors} from '../../constants';

const Styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center'},

  searchTile: {
    width: wp(78),
  },
  searchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  filterTile: {width: 40},
  righIconStyle: {
    height: 30,
    width: 30,
    marginLeft: 15,
  },
  clear: {
    color: Colors.red,
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 5,
  },
  emptyTile: {justifyContent: 'center', alignItems: 'center', marginTop: 80},
  emptyText: {fontSize: 18, fontWeight: '400'},
  dataList: {
    marginTop: 20,
  },
  Categorylist: {marginVertical: 20},
});

export default Styles;
