import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {Colors} from '../../../constants';
import {widthPercentageToDP as wp} from '../../../constants/Dimentions';

const ItemCard = (props: any) => {
  const {item, index, onPress} = props;

  const getFinalPrice = (price: number, discount: number) => {
    return (price - (price * discount) / 100).toFixed(0);
  };
  const finalPrice = getFinalPrice(item?.price, item?.discountPercentage);
  return (
    <View style={Styles.container}>
      <TouchableOpacity onPress={() => onPress?.(item, index)}>
        <View>
          <View style={Styles.imageTile}>
            <Image
              style={[Styles.imageTile]}
              resizeMode="stretch"
              source={{uri: item?.thumbnail}}
            />
          </View>
          <View style={Styles.title}>
            <Text style={Styles.titleText}>{item?.title}</Text>
            <Text style={Styles.description}>{item?.description}</Text>
            <View style={Styles.sectionTile}>
              <Text>Brand:</Text>
              <Text style={Styles.descText}>{item?.brand}</Text>
            </View>
            <View style={Styles.sectionTile}>
              <Text>Rating:</Text>
              <Text style={Styles.descText}>{item?.rating}</Text>
            </View>
            <View style={Styles.line} />
            <View style={Styles.priceTile}>
              <Text style={Styles.priceDiscount}>
                $<Text style={Styles.priceText}>{item?.price + ' '}</Text>
                {finalPrice}
              </Text>
              <View>
                <Text style={Styles.discount}>
                  {item?.discountPercentage.toFixed(0) + '% Off'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const Styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    width: wp(90),
    marginHorizontal: 20,
    backgroundColor: Colors.white,
    borderColor: Colors.shadowColor,
    shadowColor: Colors.shadowColor,
    shadowOffset: {height: 0, width: 0},
    shadowRadius: 5,
    shadowOpacity: 0.3,
    elevation: 3,
    marginBottom: 8,
    marginTop: 8,
  },
  imageTile: {
    width: wp(90),
    height: 210,
  },
  sectionTile: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    width: wp(90),
    justifyContent: 'center',
    paddingHorizontal: 16,
    marginTop: 20,
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.black,
    width: wp(90),
  },
  description: {
    fontSize: 12,
    marginTop: 8,
    marginBottom: 8,
    color: Colors.textColor,
  },
  descText: {
    fontSize: 12,
    color: Colors.textColor,
    marginLeft: 5,
  },
  line: {
    height: 1,
    backgroundColor: '#00000029',
    marginTop: 8,
  },

  priceTile: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
    marginBottom: 15,
  },
  priceText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.textColor,
    textDecorationLine: 'line-through',
  },
  priceDiscount: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.black,
  },
  discount: {
    fontSize: 16,
    color: Colors.red,
  },
});

export default ItemCard;
