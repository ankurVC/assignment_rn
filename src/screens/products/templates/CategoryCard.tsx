import React from "react";
import { View, Text, StyleSheet, Pressable } from "react-native";
import { Colors } from "../../../constants";

const CategoryCard = (props: any) => {
  const { item, onPress } = props;

  return (
    <View style={styles.container}>
      <Pressable
        style={[
          styles.tab,
          {
            backgroundColor: item?.isSelected
              ? Colors.primaryColor
              : Colors.white,
          },
        ]}
        onPress={() => onPress(item?.category)}
      >
        <Text style={{ color: item?.isSelected ? Colors.white : Colors.black }}>
          {item?.category}
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  tab: {
    height: 40,
    borderRadius: 50,
    padding: 10,
    margin: 5,
    borderWidth: 1,
    borderColor: Colors.shadowColor,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default CategoryCard;
