import React, {useState} from 'react';
import {View, StyleSheet, Image, TextInput} from 'react-native';
import {Colors, Images} from '../../../constants';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../../../constants/Dimentions';

const Search = (props: any) => {
  const {onChange} = props;
  const [value, setValue] = useState('');
  return (
    <View style={styles.container}>
      <View>
        <Image source={Images.search_icon} style={styles.searchIcon} />
      </View>
      <TextInput
        style={styles.searchInput}
        placeholderTextColor={Colors.black}
        onChangeText={text => {
          setValue(text);
          onChange(text);
        }}
        value={value}
        placeholder="Search"
        keyboardType="default"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(95),
    height: hp(6),
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.black,
  },
  searchIcon: {
    width: 14,
    height: 14,
    right: hp(1),
    tintColor: Colors.black,
  },
  searchInput: {
    width: wp(85),
    padding: wp(2),
    height: 30,
    fontSize: 14,
    color: Colors.black,
  },
});

export default Search;
