import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Colors} from '../../../constants';
import {widthPercentageToDP as wp} from '../../../constants/Dimentions';

const ItemCard = (props: any) => {
  const {item} = props;

  const getFinalPrice = (price: number, discount: number) => {
    return (price - (price * discount) / 100).toFixed(0);
  };
  const finalPrice = getFinalPrice(item?.price, item?.discountPercentage);
  return (
    <View style={Styles.container}>
      <View>
        <View style={Styles.title}>
          <Text style={Styles.titleText}>{item?.title}</Text>
          <View style={Styles.priceTile}>
            <Text style={Styles.priceDiscount}>
              $<Text style={Styles.priceText}>{item?.price + ' '}</Text>
              {finalPrice}
            </Text>
            <View style={Styles.discountTile}>
              <Text style={Styles.discount}>
                {item?.discountPercentage.toFixed(0) + '% Off'}
              </Text>
            </View>
          </View>
          <Text style={Styles.titleText}>Product Details</Text>
          <Text style={Styles.description}>{item?.description}</Text>
          <View style={Styles.sectionTile}>
            <Text>Brand:</Text>
            <Text style={Styles.descText}>{item?.brand}</Text>
          </View>
          <View style={Styles.sectionTile}>
            <Text>Rating:</Text>
            <Text style={Styles.descText}>{item?.rating}</Text>
          </View>
          <View style={{marginTop: 20, marginBottom: 30}}>
            {item?.stock > 10 ? (
              <Text style={Styles.inStock}>In stock</Text>
            ) : item?.stock == 0 ? (
              <Text style={Styles.outStock}>Out of stock</Text>
            ) : item?.stock <= 10 ? (
              <Text style={Styles.fewStock}>Few left</Text>
            ) : null}
          </View>
        </View>
      </View>
    </View>
  );
};
const Styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    width: wp(98),
    paddingHorizontal: 5,
    marginTop: 10,
  },

  sectionTile: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    justifyContent: 'center',
    paddingHorizontal: 16,
    marginTop: 20,
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colors.black,
  },
  description: {
    fontSize: 16,
    marginTop: 8,
    marginBottom: 8,
    color: Colors.textColor,
  },
  descText: {
    fontSize: 16,
    color: Colors.textColor,
    marginLeft: 5,
  },

  priceTile: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: 15,
  },
  priceText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colors.textColor,
    textDecorationLine: 'line-through',
  },
  priceDiscount: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colors.black,
  },
  discount: {
    fontSize: 14,
    width: 40,
    color: Colors.white,
    textAlign: 'center',
  },
  discountTile: {
    height: 55,
    width: 55,
    borderRadius: 30,
    backgroundColor: Colors.red,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inStock: {
    color: Colors.green,
    fontSize: 22,
    fontWeight: 'bold',
  },
  outStock: {
    color: Colors.red,
    fontSize: 22,
    fontWeight: 'bold',
  },
  fewStock: {
    color: Colors.orange,
    fontSize: 22,
    fontWeight: 'bold',
  },
});

export default ItemCard;
