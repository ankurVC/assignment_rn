import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from '../../constants/Dimentions';

const Styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center'},
  imageContainer: {justifyContent: 'center', alignItems: 'center'},
  cardImage: {
    height: 300,
    width: wp(90),
  },
  pagination: {marginTop: -10},
  dots: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.92)',
  },
});

export default Styles;
