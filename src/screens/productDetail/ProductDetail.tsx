import React, {FC, useEffect} from 'react';
import {View, Image, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {AppStackProps} from '../../@types';
import {NavigationScreens} from '../../navigations/NavigationScreens';
import {Header} from '../../components';
import {Images} from '../../constants';
import Styles from './Styles';
import {widthPercentageToDP as wp} from '../../constants/Dimentions';
import {DetailCard} from './templates';

type ProductDetailScreenNavigationType = NativeStackNavigationProp<
  AppStackProps,
  NavigationScreens.ProductDetail
>;

const ProductDetail: FC = (props: any) => {
  const navigation = useNavigation<ProductDetailScreenNavigationType>();
  const productData = props?.route?.params;
  const [index, setIndex] = React.useState(0);
  const isCarousel = React.useRef(null);

  useEffect(() => {
    let focusListener = navigation.addListener('focus', async () => {});
    return focusListener;
  }, [navigation]);

  const _renderItem = ({item}: any) => {
    return (
      <View style={Styles.imageContainer}>
        <Image
          source={{uri: item}}
          style={Styles.cardImage}
          resizeMode={'contain'}
        />
      </View>
    );
  };
  const pagination = () => {
    return (
      <View style={Styles.pagination}>
        <Pagination
          dotsLength={productData?.item?.images.length}
          activeDotIndex={index}
          carouselRef={isCarousel}
          dotStyle={Styles.dots}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          tappableDots={true}
        />
      </View>
    );
  };

  return (
    <View style={Styles.container}>
      <Header
        onPressBack={() => navigation.goBack()}
        leftImage={Images.arrow_left}
        headerTitle={'Product Detail'}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Carousel
          ref={isCarousel}
          layout={'default'}
          data={productData?.item?.images}
          renderItem={_renderItem}
          sliderWidth={wp(100)}
          itemWidth={wp(100)}
          inactiveSlideShift={0}
          onSnapToItem={(itemIndex: any) => setIndex(itemIndex)}
          loop={true}
          useScrollView={true}
        />
        {pagination()}
        <DetailCard item={productData?.item} />
      </ScrollView>
    </View>
  );
};

export default ProductDetail;
