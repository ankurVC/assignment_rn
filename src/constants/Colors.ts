const Colors = {
  primaryColor: '#775FFF',
  white: '#fff',
  black: '#000',
  red: '#FF6D00',
  orange: '#FFA500',
  green: '#50C878',
  shadowColor: '#B0A3F7',
  textColor: '#00000070',
};
export default Colors;
