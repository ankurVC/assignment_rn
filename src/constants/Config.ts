const Config = {
  API_URL: 'https://dummyjson.com/',
  PRODUCTS: 'products',
};
export const SUCCESS = 'success';
export const FAILURE = 'failure';
export default Config;
