import Colors from './Colors';
import Images from './Images';
import Config from './Config';

export { Colors, Images, Config };
