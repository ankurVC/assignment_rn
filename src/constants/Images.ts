const Images = {
  arrow_left: require('../../assets/images/arrow_left.png'),
  close_icon: require('../../assets/images/close_icon.png'),
  search_icon: require('../../assets/images/search_icon.png'),
  filter_icon: require('../../assets/images/filter_icon.png'),
};
export default Images;
