import AxiosInstance from './Intercepter';
import Config from '../constants/Config';

export async function get(api: any) {
  return AxiosInstance.get(`${Config.API_URL}${api}`)
    .then((res: any) => {
      if (res.status == 200 && !res.data.status) {
        return {
          data: res.data,
          status: 'success',
        };
      }
      return res.data;
    })
    .catch((err: any) => err);
}
