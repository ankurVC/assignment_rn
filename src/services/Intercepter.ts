import axios from 'axios';
import Config, {FAILURE} from '../constants/Config';

// Create Instance
const AxiosInstance = axios.create({
  baseURL: Config.API_URL,
  timeout: 30000,
  transformRequest: [
    function (data) {
      if (data && data._parts) {
        return data;
      } else {
        return JSON.stringify(data);
      }
    },
  ],
  headers: {'Content-Type': 'application/json'},
});

// Response Interceptor
AxiosInstance.interceptors.response.use(
  (response: any) => {
    return response;
  },
  (error: any) => {
    if (!error.response) {
      return Promise.reject({
        status: FAILURE,
        message: 'Please check your internet connection',
      });
    } else {
      return error.response;
    }
  },
);

export default AxiosInstance;
