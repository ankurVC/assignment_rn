## Assignment-App

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Running App](#runningApp)
- [Reference Links](#referenceLinks)

<br/>

## About The Project

This App Mobile version targeting Both Android and iOS platforms

<br/>

### Built With

- [React Native](https://reactnative.dev/)
- [React Navigation](https://reactnavigation.org/)
- [Typescript]
- [TailwindCSS]
  <br/>

## Getting Started

Please make sure, you installed all required Prerequisites before installing this repo

Basic dependencies below:

```
OS: macOS Catalina and above
Node:
npm:
Xcode:
Android studio
Gradle:
```

<br/>

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

**Android**

```sh
Download and install Android Studio. While on Android Studio intallation wizard, make sure the boxes next to all of the following items are checked:

    - Android SDK
    - Android SDK Platform

    - Android Virtual Device
    If you are not already using Hyper-V: Performance (Intel ® HAXM) (See here for AMD or Hyper-V)
```

<br/>

**iOS**

The easiest way to install Xcode is via the Mac App Store. Installing Xcode will also install the iOS Simulator and all the necessary tools to build your iOS app.

If you have already installed Xcode on your system, make sure it is version 9.4 or newer.

**CocoaPods**

CocoaPods is built with Ruby and it will be installable with the default Ruby available on macOS.

```sh
sudo gem install cocoapods
```

Then you will need to install all the pods:

```sh
cd ios
pod install
```

<br/>

### Important Note on Java, Gradle and Node

**Java SDK 8**

This project requires Java 8 SDK. It will work on higher versions as well, so set up JAVA_HOME environment variable to point to a valid Java 8 SDK path, also addresses the issue if you have multiple Java SDKs installed:

On MacOS, typically Java SDKs are installed in /Library/Java/JavaVirtualMachines. Assuming your Java SDK directory is /Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk (Java SDK 8), set up this line this in your ~/.bash_profile file:

```sh
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk/Contents/Home
```

**Gradle**

Please use Gradle version 7.5.1 On MacOS, you can install it using sdkman (https://sdkman.io/install).

Once sdkman is installed:

```sh
sdk install gradle 7.5.1
sdk default gradle 7.5.1
```

**Node.js & NPM**

On MacOS, you can install using nvm and brew (https://brew.sh/):

Install nvm:

```sh
brew install nvm
```

Add this to your ~/.bash_profile:

```sh
export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
```

<br/>

### Code Installation

```sh
1. Clone the repo

2. To install all project dependencies: npm ci or npm install
```

<br/>

### Running App

After installing dependencies follow the below commands to run App on emulators/simulator

```sh
Start : npx react-native start or npx react-native run-android or npx react native run-ios
```

<br/>

### Building the app

**Android**

```sh
cd android
./gradlew assembleRelease

```

OR

```sh
cd android
./gradlew bundleRelease
```

<br/>

**iOS**

Note: For IOS you need to configure profile and certificates using your Apple ID account).

<br/>

### Reference Links

- React Native CLI setup - https://reactnative.dev/docs/environment-setup
- React Navigation - https://reactnavigation.org/docs/getting-started

<br />
